set $mod Mod4

font pango:Hack Nerd Font 10

### STARTUP ###

exec --no-startup-id dex --autostart --environment i3
exec --no-startup-id xss-lock --transfer-sleep-lock -- i3lock --nofork
exec --no-startup-id nm-applet
exec --no-startup-id ~/.screenlayout/home.sh # arandr config
exec --no-startup-id dunst
exec --no-startup-id nitrogen --restore

### VOLUME ###

bindsym XF86AudioRaiseVolume exec --no-startup-id pamixer -i 5
bindsym XF86AudioLowerVolume exec --no-startup-id pamixer -d 5
bindsym XF86AudioMute exec --no-startup-id pamixer --toggle-mute

### FLOATING ###

# Use Mouse+$mod to drag floating windows to their wanted position
floating_modifier $mod

### KEYBINDINGS ###

# start a terminal
bindsym $mod+Return exec kitty

# kill focused window
bindsym $mod+w kill

# rofi
bindsym $mod+d exec --no-startup-id "rofi -show drun"

# change focus
bindsym $mod+j focus left
bindsym $mod+k focus down
bindsym $mod+l focus up
bindsym $mod+semicolon focus right

# split horizontal/vertical
bindsym $mod+h split h
bindsym $mod+v split v

# enter fullscreen mode for the focused container
bindsym $mod+f fullscreen toggle

### WORKSPACES ###

# Define names for default workspaces for which we configure key bindings later on.
# We use variables to avoid repeating the names in multiple places.
set $ws1 "1:www"
set $ws2 "2:dev"
set $ws3 "3:music"
set $ws4 "4:chat"
set $ws5 "5:notes"
set $ws6 "6"
set $ws7 "7"
set $ws8 "8"
set $ws9 "9"
set $ws10 "10"

# switch to workspace
bindsym $mod+1 workspace number $ws1
bindsym $mod+2 workspace number $ws2
bindsym $mod+3 workspace number $ws3
bindsym $mod+4 workspace number $ws4
bindsym $mod+5 workspace number $ws5
bindsym $mod+6 workspace number $ws6
bindsym $mod+7 workspace number $ws7
bindsym $mod+8 workspace number $ws8
bindsym $mod+9 workspace number $ws9
bindsym $mod+0 workspace number $ws10

# move focused container to workspace
bindsym $mod+Shift+1 move container to workspace number $ws1
bindsym $mod+Shift+2 move container to workspace number $ws2
bindsym $mod+Shift+3 move container to workspace number $ws3
bindsym $mod+Shift+4 move container to workspace number $ws4
bindsym $mod+Shift+5 move container to workspace number $ws5
bindsym $mod+Shift+6 move container to workspace number $ws6
bindsym $mod+Shift+7 move container to workspace number $ws7
bindsym $mod+Shift+8 move container to workspace number $ws8
bindsym $mod+Shift+9 move container to workspace number $ws9
bindsym $mod+Shift+0 move container to workspace number $ws10

# reload the configuration file
bindsym $mod+Shift+c reload

# restart i3 inplace (preserves your layout/session, can be used to upgrade i3)
bindsym $mod+Shift+r restart

# lock (locks your screen)
bindsym $mod+Shift+l exec i3lock

### RESIZE ###

mode "resize" {
		# pressing will resize window
        bindsym j resize shrink width 10 px or 10 ppt
        bindsym k resize grow height 10 px or 10 ppt
        bindsym l resize shrink height 10 px or 10 ppt
        bindsym semicolon resize grow width 10 px or 10 ppt

        # same bindings, but for the arrow keys
        bindsym Left resize shrink width 10 px or 10 ppt
        bindsym Down resize grow height 10 px or 10 ppt
        bindsym Up resize shrink height 10 px or 10 ppt
        bindsym Right resize grow width 10 px or 10 ppt

        # back to normal: Enter or Escape or $mod+r
        bindsym Return mode "default"
        bindsym Escape mode "default"
        bindsym $mod+r mode "default"
}

bindsym $mod+r mode "resize"

### BAR ###

bar {
	status_command i3status
	position top
	tray_output VGA-1
    colors {
		background #000000
		statusline #ffffff
		separator #666666
#                      border  backgr  text
		focused_workspace  #0A84FFFF #0A84FFFF #ffffff
		active_workspace   #212124 #212124 #ffffff
		inactive_workspace #1C1C1E #1C1C1E #ffffff
		urgent_workspace   #2f343a #900000 #ffffff
		binding_mode       #2f343a #900000 #ffffff
	}
}

### COLORS ###

# class                 border  backgr. text    indicator child_border
client.focused          #0A84FFFF #0A84FFFF #ffffff #0A84FFFF   #0A84FFFF
client.focused_inactive #333333 #5f676a #ffffff #484e50   #5f676a
client.unfocused        #1C1C1E #1C1C1E #ffffff #1C1C1E   #1C1C1E
client.urgent           #2f343a #900000 #ffffff #900000   #900000
client.placeholder      #000000 #0c0c0c #ffffff #000000   #0c0c0c

client.background       #ffffff
